﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOC2018.Challenges
{
    public static class Day6
    {
        public static int GRID_HEIGHT = 500;
        public static int GRID_WIDTH = 500;
        public static List<Point> Points = new List<Point>();
        public static char[,] Grid;
        public static void Solve()
        {



            var input = File.ReadAllLines("../../Inputs/Day6.txt");
            int i = 0;
            foreach (var line in input)
            {
                char id = (i <= 25) ? (char)(i + 65) : (char)(i + 71); // covers 52 points! our input is 50!
                var arr_Line = line.Split(',').Select(x => int.Parse(x)).ToArray();
                var point = new Point(arr_Line[0], arr_Line[1], id);
                Points.Add(point);
                i++;
            }

            var largestX = Points.Max(x => x.X) + 1;
            var largestY = Points.Max(x => x.Y) + 1;

            if (largestX > largestY)
            {
                GRID_HEIGHT = largestX;
                GRID_WIDTH = largestX;
            }
            else
            {
                GRID_HEIGHT = largestY;
                GRID_WIDTH = largestY;
            }

            Grid = new char[GRID_WIDTH, GRID_HEIGHT];


            foreach (var point in Points)
            {
                Grid[point.X, point.Y] = point.C;

            }

            FillGrid();
            //DrawGrid();
            var result = GetLargestFiniteArea();
            GetSafeRegion();
            Console.ReadLine();

        }
        public static void DrawGrid()
        {
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                for (int j = 0; j < GRID_HEIGHT; j++)
                {

                    if (Points.Any(p => p.X == j && p.Y == i))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    Console.Write((Grid[j, i] == 0 ? 'X' : Grid[j, i]));
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.Write("\n");
            }
        }
        public static int CalculateMHDistance(Point a, Point b)
        {
            return Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
        }
        public static void FillGrid()
        {
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                for (int j = 0; j < GRID_HEIGHT; j++)
                {
                    var cellChar = Grid[j, i];

                    if (cellChar == 0)
                    {
                        var point = GetClosestPoint(new Point(j, i, (char)0));

                        if (point == null)
                        {
                            Grid[j, i] = '.';
                        }
                        else
                        {
                            Grid[j, i] = point.C;
                        }
                    }

                }

            }
        }
        public static Point GetClosestPoint(Point source)
        {
            var smallestDistance = int.MaxValue;
            var closestPoint = new Point();
            foreach (var point in Points)
            {
                var distance = CalculateMHDistance(source, point);

                if (distance == smallestDistance)
                {
                    closestPoint = null;
                }

                if (distance < smallestDistance)
                {
                    smallestDistance = distance;
                    closestPoint = point;
                }
            }


            return closestPoint;
        }
        public static List<Point> GetFinitePoints()
        {

            var result = new List<Point>();

            foreach (var point in Points)
            {
                bool isFinite = true;

                // check top row
                for (int i = 0; i < GRID_WIDTH && isFinite; i++)
                {
                    if (Grid[i, 0] == point.C)
                    {
                        isFinite = false;

                    }
                }

                // check Bottom row
                for (int i = 0; i < GRID_WIDTH && isFinite; i++)
                {
                    if (Grid[i, GRID_HEIGHT - 1] == point.C)
                    {
                        isFinite = false;

                    }
                }

                // check west wall

                for (int i = 0; i < GRID_HEIGHT && isFinite; i++)
                {
                    if (Grid[0, i] == point.C)
                    {
                        isFinite = false;

                    }
                }
                // check east wall
                for (int i = 0; i < GRID_HEIGHT && isFinite; i++)
                {
                    if (Grid[GRID_WIDTH - 1, i] == point.C)
                    {
                        isFinite = false;

                    }
                }

                if (isFinite)
                {
                    result.Add(point);
                }


            }

            return result;

        }
        public static int GetLargestFiniteArea()
        {
            var finitePoints = GetFinitePoints();

            foreach (var point in finitePoints)
            {

                for (int i = 0; i < GRID_WIDTH; i++)
                {
                    for (int j = 0; j < GRID_HEIGHT; j++)
                    {
                        if (Grid[j, i] == point.C)
                        {
                            point.Count++;

                        }
                    }

                }


            }
            finitePoints = finitePoints.OrderByDescending(x => x.Count).ToList();
            var result = finitePoints.OrderByDescending(x => x.Count).First().Count;
            return result;
        }
        public static void GetSafeRegion()
        {
            var regionSize = 0;
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                for (int j = 0; j < GRID_HEIGHT; j++)
                {
                    if (GetTotalDistanceFromAllPoints(new Point(j, i, (char)0)) < 10000)
                    {
                        regionSize++;
                    }


                }

            }

        }

        public static int GetTotalDistanceFromAllPoints(Point source)
        {
            var totalDistance = 0;


            foreach (var point in Points)
            {
                totalDistance += CalculateMHDistance(source, point);
            }

            return totalDistance;
        }
    }


    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public char C { get; set; }
        public int Count { get; set; }
        public Point(int x, int y, char c)
        {
            X = x;
            Y = y;
            C = c;
        }
        public Point()
        {

        }
    }

}
