﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOC2018.Challenges
{
    public static class Day7
    {
        public static List<Step> Steps = new List<Step>();
        public static void Solve()
        {
            var input = File.ReadAllLines("../../Inputs/Day7.txt");
            string s_Steps = "";
            string orderOfCompletion = "";

            foreach (var line in input)
            {
                var t_line = line.Replace("must be finished before step", "").Replace("can begin.", "").Replace("Step ", "").TrimEnd(' ');
                var arr_line = t_line.Trim(' ').Split(' ');

                var firstValue = arr_line[0];
                var secondValue = arr_line[2];

                s_Steps += firstValue + secondValue;

            }

            s_Steps = RemoveDuplicates(s_Steps);


            foreach (var ch in s_Steps)
            {
                Steps.Add(new Step()
                {
                    Id = ch.ToString()
                });
            }
            SetupRequirments(input);

            var currentStep = Steps.First();
            Step previousStep;
            int i = 0;
            while (!currentStep.IsCompleted)
            {
                if (currentStep.Requirments.Count(x => !x.IsCompleted) == 0)
                {
                    currentStep.IsCompleted = true;
                    orderOfCompletion += currentStep.Id;
                    previousStep = currentStep;
                    i++;
                    currentStep = currentStep.PotentialNextSteps.FirstOrDefault();

                }
            }

        }
        public static string RemoveDuplicates(string input)
        {
            return new string(input.ToCharArray().Distinct().ToArray());
        }
        public static void SetupRequirments(string[] input)
        {
            foreach (var line in input)
            {
                var t_line = line.Replace("must be finished before step", "").Replace("can begin.", "").Replace("Step ", "").TrimEnd(' ');
                var arr_line = t_line.Trim(' ').Split(' ');

                var requirement = arr_line[0];
                var id = arr_line[2];

                var step = Steps.SingleOrDefault(x => x.Id == id);


                step.Requirments.Add(Steps.SingleOrDefault(x => x.Id == requirement));



            }
        }
    }

    public class Step
    {
        public string Id { get; set; }
        public List<Step> Requirments { get; set; }
        public bool IsCompleted { get; set; }
        public List<Step> PotentialNextSteps
        {
            get
            {

                var pSteps = Day7.Steps.Where(x => x.Requirments.Any(y => y.Id == this.Id)).OrderBy(z => z.Id).ToList();
                pSteps = pSteps.Where(x => !x.Requirments.Any(y => !y.IsCompleted)).ToList();
                return pSteps;

            }
        }

        public Step()
        {
            Requirments = new List<Step>();
        }
    }
}
