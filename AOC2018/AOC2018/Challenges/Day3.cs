﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOC2018.Challenges
{
    public static class Day3
    {

        public static void Solve()
        {
            var input = File.ReadAllLines("../../Inputs/Day3.txt");

            int[,] grid = new int[1000, 1000];

            foreach (var line in input)
            {

                var arr_line = line.Split(' ');

                var id = int.Parse(arr_line[0].TrimStart('#'));
                var posx = int.Parse(arr_line[2].Split(',')[0]);
                var posy = int.Parse(arr_line[2].Split(',')[1].TrimEnd(':'));
                var width = int.Parse(arr_line[3].Split('x')[0]);
                var height = int.Parse(arr_line[3].Split('x')[1]);

                for (int x = posx; x < posx + width; x++)
                {
                    for (int y = posy; y < posy + height; y++)
                    {
                        grid[x, y]++;
                    }
                }

            }


            // now get the number of cells filled in
            var count = 0;
            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (grid[i, j] > 1)
                        count++;

                }
            }


            foreach (var line in input)
            {

                var arr_line = line.Split(' ');

                var id = int.Parse(arr_line[0].TrimStart('#'));
                var posx = int.Parse(arr_line[2].Split(',')[0]);
                var posy = int.Parse(arr_line[2].Split(',')[1].TrimEnd(':'));
                var width = int.Parse(arr_line[3].Split('x')[0]);
                var height = int.Parse(arr_line[3].Split('x')[1]);
                bool claimIntact = true;
                for (int x = posx; x < posx + width; x++)
                {
                    for (int y = posy; y < posy + height; y++)
                    {
                        if(grid[x, y] > 1)
                        {
                            claimIntact = false;
                        }
                    }
                }

                if (claimIntact)
                {

                }

            }


        }
    }
}
