﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AOC2018.Challenges
{
    public static class Day2
    {

        public static void SolvePart2()

        {
            var input = File.ReadAllLines("../../Inputs/Day2.txt");


            for (int i = 0; i < input.Length; i++)
            {

                for (int j = 0; j < input.Length; j++)
                {

                    if (Compute(input[i], input[j]) == 1)
                    {
                        var result = "";
                        for (int k = 0; k < input[i].Length; k++)
                        {
                            if(input[i][k] == input[j][k])
                            {
                                result += input[i][k];
                            }
                        }


                    }
                }

            }


        }


        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        public static int Compute(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

    }
}
