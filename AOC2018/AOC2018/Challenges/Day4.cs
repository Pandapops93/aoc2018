﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AOC2018.Challenges
{

    public static class Day4
    {
        public static List<Guard> Guards { get; set; }
        public static List<Event> Events { get; set; }
        public static Guard PrevGuard { get; set; }
        public static void Solve()
        {
            var input = File.ReadAllLines("../../Inputs/Day4.txt");
            Guards = new List<Guard>();
            Events = new List<Event>();
            var rawData = new List<rawInput>();
            Guard currGuard = new Guard(-1);
            foreach (var line in input)
            {
                var s_Time = Regex.Match(line, @"\[([^]]+)\]").Groups[0].Value.TrimEnd(']').TrimStart('[');
                var time = DateTime.Parse(s_Time);

                rawData.Add(new rawInput()
                {
                    date = time,
                    Data = line
                });
            }

            foreach (var data in rawData.OrderBy(x => x.date))
            {
                var s_Time = Regex.Match(data.Data, @"\[([^]]+)\]").Groups[0].Value.TrimEnd(']').TrimStart('[');
                var time = DateTime.Parse(s_Time);
                var guardId = GetGuardId(data.Data);

                if (GetAction(data.Data) == "begins shift")
                {

                    currGuard = Guards.SingleOrDefault(x => x.Id == guardId);
                    if (currGuard == null)
                    {
                        currGuard = new Guard(guardId);
                        Guards.Add(currGuard);
                    }
                }



                var ev = new Event(currGuard, time, data.Data, GetAction(data.Data));
                Events.Add(ev);

            }

            var sleepiestGuard = GetGuardWhoSleptMostOnSameMinute();

            var result = sleepiestGuard.Id * sleepiestGuard.GetMostSleptMinute();


        }

        public static int GetGuardId(string line)
        {

            var start = line.IndexOf("#") + 1;
            var end = line.IndexOf(' ', start);


            var id = line.Substring(start, end - start);


            int result = 0;

            int.TryParse(id, out result);
            return result;
        }
        public static string GetAction(string line)
        {

            if (line.Contains("begins shift"))
                return "begins shift";

            if (line.Contains("wakes up"))
                return "wakes up";

            if (line.Contains("falls asleep"))
                return "falls asleep";

            return "";
        }
        public static Guard GetSleepiestGuard()
        {

            var sleepiestGuard = new Guard(-1);

            foreach(var guard in Guards)
            {
                if (guard.GetTotalTimeSlept() > sleepiestGuard.GetTotalTimeSlept())
                    sleepiestGuard = guard;
            }

            return sleepiestGuard;

        }
        public static Guard GetGuardWhoSleptMostOnSameMinute()
        {
            var sleepiestGuard = new Guard(-1);

            foreach (var guard in Guards)
            {
                if (guard.GetMostSleptMinuteValue() > sleepiestGuard.GetMostSleptMinuteValue())
                    sleepiestGuard = guard;
            }

            return sleepiestGuard;
        }

    }

    public class rawInput
    {
        public DateTime date { get; set; }
        public string Data { get; set; }
    }

    public class Event
    {
        public DateTime Time { get; set; }
        public int Minutes
        {
            get
            {
                return Time.Minute;
            }
        }
        public Guard Guard { get; set; }
        public string RawData { get; set; }
        public string Action { get; set; }
        public Event(Guard guard, DateTime time, string raw, string action)
        {
            this.Time = time;
            this.RawData = raw;
            this.Guard = guard;
            this.Action = action;

            if (!this.Guard.Schedule.ContainsKey(time.Date))
            {
                this.Guard.Schedule.Add(time.Date, new int[60]);
            }

            switch (action)
            {
                case "falls asleep":
                    this.Guard.Schedule[time.Date][Minutes] = 1;
                    break;
                case "begins shift":
                    break;
                case "wakes up":
                    this.Guard.Schedule[time.Date][Minutes] = 0;

                    var t_mins = Minutes - 1;

                    while (this.Guard.Schedule[time.Date][t_mins] == 0)
                    {
                        this.Guard.Schedule[time.Date][t_mins] = 1;
                        t_mins--;
                    }

                    break;

            }




        }

    }

    public class Guard
    {
        public int Id { get; set; }
        public Dictionary<DateTime, int[]> Schedule { get; set; }

        public Guard(int id)
        {
            this.Id = id;
            this.Schedule = new Dictionary<DateTime, int[]>();
        }
        public int GetMostSleptMinute()
        {
            int[] result = new int[60];
            foreach (var day in Schedule)
            {
                for (int i = 0; i < 60; i++)
                {
                    if (day.Value[i] == 1)
                        result[i]++;
                }
            }

            var maxIdx = 0;
            var max = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if(result[i] > max)
                {
                    maxIdx = i;
                    max = result[i];
                }
            }

            return maxIdx;
        }
        public int GetMostSleptMinuteValue()
        {
            int[] result = new int[60];
            foreach (var day in Schedule)
            {
                for (int i = 0; i < 60; i++)
                {
                    if (day.Value[i] == 1)
                        result[i]++;
                }
            }

            var maxIdx = 0;
            var max = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] > max)
                {
                    maxIdx = i;
                    max = result[i];
                }
            }

            return max;
        }
        public int GetTotalTimeSlept()
        {

            var total = 0;

            foreach(var day in Schedule)
            {
                for (int i = 0; i < 60; i++)
                {
                    if(day.Value[i] == 1)
                    {
                        total++;
                    }
                }
            }

            return total;
        }
    }
}
