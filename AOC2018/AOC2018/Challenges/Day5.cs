﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOC2018.Challenges
{
    public static class Day5
    {

        public static void Solve()
        {
            var input = File.ReadAllText("../../Inputs/Day5.txt");

            while (HasReactionsLeft(input))
            {
                input = RemoveReactions(input);
            }

            Console.WriteLine(input.Length);
        }

        public static void SolvePart2()
        {
            var input = File.ReadAllText("../../Inputs/Day5.txt");

            var alphabet = "abcdefghijklmnopqrstuvxyz";
            var smallestPolymer = 100000;
            foreach (char letter in alphabet)
            {
                var t_input = input;
                t_input = t_input.Replace(char.ToLower(letter).ToString(), "");
                t_input = t_input.Replace(char.ToUpper(letter).ToString(), "");

                while (HasReactionsLeft(t_input))
                {
                    t_input = RemoveReactions(t_input);
                }

                if (t_input.Length < smallestPolymer)
                    smallestPolymer = t_input.Length;

            }

        }

        public static string RemoveReactions(string input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                char a = input[i];

                if (i + 1 >= input.Length)
                    continue;

                char b = input[i + 1];

                if (IsInverse(a, b))
                {
                    input = input.Remove(i, 2);
                 //   Console.Write($"\rPair Found : {a}{b} Characters Left = {input.Length}");
                }
            }

            return input;
        }
        public static bool HasReactionsLeft(string input)
        {

            for (int i = 0; i < input.Length; i++)
            {
                char a = input[i];

                if (i + 1 >= input.Length)
                    continue;

                char b = input[i + 1];

                if (IsInverse(a, b))
                {
                    return true;
                }
            }

            return false;
        }
        public static bool IsInverse(char a, char b)
        {
            if (a != b)
            {
                if (char.ToLower(a) == char.ToLower(b))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
